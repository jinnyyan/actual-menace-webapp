// const { promisify } = require('util');
// const cheerio = require('cheerio');
// const graph = require('fbgraph');
// const { LastFmNode } = require('lastfm');
// const tumblr = require('tumblr.js');
// const GitHub = require('@octokit/rest');
// const Twit = require('twit');
// const stripe = require('stripe')(process.env.STRIPE_SKEY);
// const twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
// const clockwork = require('clockwork')({ key: process.env.CLOCKWORK_KEY });
// const paypal = require('paypal-rest-sdk');
// const lob = require('lob')(process.env.LOB_KEY);
// const ig = require('instagram-node').instagram();
// const axios = require('axios');
// const { google } = require('googleapis');
// const Quickbooks = require('node-quickbooks');
// const validator = require('validator');

Quickbooks.setOauthVersion('2.0');

/**
 * GET /api
 * List of API examples.
 */
// exports.getApi = (req, res) => {
//   res.render('loader', {
//     title: 'Loader'
//   });
// };

/**
 * GET /api/chart
 * Chart example.
 */
// exports.getChart = async (req, res, next) => {
//   const url = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=compact&apikey=${process.env.ALPHA_VANTAGE_KEY}`;
//   axios.get(url)
//     .then((response) => {
//       const arr = response.data['Time Series (Daily)'];
//       let dates = [];
//       let closing = []; // stock closing value
//       const keys = Object.getOwnPropertyNames(arr);
//       for (let i = 0; i < 100; i++) {
//         dates.push(keys[i]);
//         closing.push(arr[keys[i]]['4. close']);
//       }
//       // reverse so dates appear from left to right
//       dates.reverse();
//       closing.reverse();
//       dates = JSON.stringify(dates);
//       closing = JSON.stringify(closing);
//       res.render('api/chart', {
//         title: 'Chart',
//         dates,
//         closing
//       });
//     }).catch((err) => {
//       next(err);
//     });
// };


/**
 * GET /loader
 * File Upload API example.
 */

exports.getFileUpload = (req, res) => {
  res.render('/loader', {
    title: 'File Upload'
  });
};

exports.postFileUpload = (req, res) => {
  req.flash('success', { msg: 'File was uploaded successfully.' });
  res.redirect('/loader');
};
