/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  res.render('home', {
    title: 'Home'
  });
};

/**
 * GET /send-data
 */
exports.dataRetrieval = (req, res) => {
  console.log(req.body);
  
  // var f = "home.html" 

  var newPage = res.render('home', { 
    title: 'Data received!',
    message: JSON.stringify(req.body) 
  });

  // req.get({url: 'http://localhost:8080', headers: req.headers});

  // processRequest(req);
  // res.setHeader('Content-Type', 'application/json');
  // res.send('Req OK');
  // writeTextFile(f, newPage)

  // function writeTextFile(afilename, output)
  // {
  //   var txtFile =new File(afilename);
  //   txtFile.writeln(output);
  //   txtFile.close();
  // }
};
